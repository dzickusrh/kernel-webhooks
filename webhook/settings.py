"""Initialize the message queue."""
import json
import os

from cki_lib.messagequeue import MessageQueue

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST', 'localhost')
RABBITMQ_PORT = int(os.environ.get('RABBITMQ_PORT', "5672"))
RABBITMQ_USER = os.environ.get('RABBITMQ_USER', 'guest')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD', 'guest')

WEBHOOKS_EXCHANGE = 'webhooks'
BUGZILLA_QUEUE = 'bugzilla'

QUEUE = MessageQueue(RABBITMQ_HOST, RABBITMQ_PORT,
                     RABBITMQ_USER, RABBITMQ_PASSWORD)

GITLAB_TOKENS = {
    h: os.environ.get(t)
    for h, t in json.loads(os.environ.get('GITLAB_INSTANCES', '{}')).items()
}

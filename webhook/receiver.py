"""Route webhook messages to an amqp queue."""
import os
import re
from urllib.parse import urlsplit

from flask import abort
from flask import Flask
from flask import request

from . import settings

app = Flask(__name__)


def check_websecret():
    """Check the GitLab websecret token."""
    key = os.environ.get('WEBHOOK_RECEIVER_WEBSECRET')
    if not key:
        abort(404)
    try:
        secret = request.headers["X-Gitlab-Token"]
    except KeyError:
        abort(403, "Permission denied: missing webhook token")

    if secret != key:
        abort(403, "Permission denied: invalid webhook token")


@app.route('/', methods=['POST'])
def webhook():
    """Process a webhook."""
    check_websecret()

    data = request.json
    object_kind = data['object_kind']
    if 'project' in data:
        web_url = data['project']['web_url']
    else:
        web_url = data['repository']['homepage']
    web_url = urlsplit(web_url)
    topic = re.sub('[./]+', '.',
                   f'{web_url.hostname}/{web_url.path}/{object_kind}')

    settings.QUEUE.send_message(data, topic, settings.WEBHOOKS_EXCHANGE)
    return "OK"

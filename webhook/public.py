"""
Web Hook handlers.

To add a handler for a new event type, add a new function to the module and
register it to the :data:`WEBHOOKS` dictionary.
"""
import os

from cki_lib import logger

from . import settings
from .queue import Message

CONFIG_LABEL = "Configuration"

LOGGER = logger.get_logger('cki.webhook.public')


def _drop_ack_nack_labels(payload: dict, gl_mergerequest):
    """If the merge request got updated, clear any Acked-by/Nacked-by tags."""
    if payload["object_attributes"]["action"] != "update":
        LOGGER.debug("Skipping clearing acks/nacks since %r isn't updated",
                     gl_mergerequest)
        return
    if "oldrev" not in payload["object_attributes"]:
        LOGGER.debug(
            "Merge request %r got updated, but no code changes were made so "
            "ack/naks are still valid",
            gl_mergerequest,
        )
        return

    gl_mergerequest.labels = [
        label for label in gl_mergerequest.labels
        if "Acked-by" not in label and "Nacked-by" not in label]
    gl_mergerequest.save()


def _apply_subsystem_label(gl_project, gl_mergerequest):
    """Apply a subsystem label to configuration merge requests.

    This is intended to run in a thread as it can take a long time to perform a
    code search and web hooks need to respond quickly. It's possible we'll miss
    labeling some merge requests if the process is killed after acking the web
    hook and before finishing this, but it's not a big deal.

    An entirely new GitLab client is created as the underlying requests session
    is not thread-safe.
    """
    subsystem_labels = set()
    for change in gl_mergerequest.changes()["changes"]:
        file_name = os.path.basename(change["new_path"])
        if file_name.startswith("CONFIG_"):
            results = gl_project.search("blobs", f"config {file_name[7:]}")
            for path in [
                    os.path.dirname(p["path"]) for p in results
                    if "Kconfig" in p["path"]]:
                # if drivers/net/ethernet/ do 4 parts at most
                # otherwise do 2 parts at most
                if path.startswith("drivers/net/ethernet/"):
                    subsystem_labels.update(
                        [f"Subsystem: {subsys}" for subsys in path.split(
                            "/")[:4]])
                else:
                    subsystem_labels.update(
                        [f"Subsystem: {subsys}" for subsys in path.split(
                            "/")[:2]])

    if subsystem_labels:
        gl_mergerequest.labels += list(subsystem_labels)
        gl_mergerequest.save()


def _apply_config_label(gl_mergerequest) -> None:
    """Add or remove the CONFIG_LABEL to merge requests."""
    config_dirs = [
        f"redhat/configs/{flavor}" for flavor in ("fedora", "common", "ark")]
    for change in gl_mergerequest.changes()["changes"]:
        for config_dir in config_dirs:
            if change["old_path"].startswith(config_dir) or change[
                    "new_path"
            ].startswith(config_dir):
                gl_mergerequest.labels.append(CONFIG_LABEL)
                gl_mergerequest.save()
                return

    # Nothing touches the configuration so ensure the label is *not* applied.
    if CONFIG_LABEL in gl_mergerequest.labels:
        gl_mergerequest.labels = [
            label for label in gl_mergerequest.labels if label != CONFIG_LABEL
        ]
        gl_mergerequest.save()


def process_merge_request(payload):
    """Process a merge request message.

    Web hook for when a new merge request is created, an existing merge request
    was updated/merged/closed or a commit is added in the source branch

    The request body is a JSON document documented at
    https://docs.gitlab.com/ce/user/project/integrations/webhooks.html
    """
    message = Message(payload)

    with message.gl_instance() as gl_instance:
        gl_project = gl_instance.projects.get(payload["project"]["id"])
        gl_mergerequest = gl_project.mergerequests.get(
            payload["object_attributes"]["iid"])

        _apply_config_label(gl_mergerequest)
        _apply_subsystem_label(gl_project, gl_mergerequest)


# pylint: disable=unused-argument
def process_message(routing_key, payload):
    """Process a webhook message."""
    object_kind = payload.get('object_kind')
    if not object_kind:
        LOGGER.warning('Missing object_kind in message')
        return False  # unit tests
    if object_kind not in WEBHOOKS:
        LOGGER.warning('No handler found for %s', object_kind)
        return False  # unit tests
    WEBHOOKS[object_kind](payload)
    return True  # unit tests


# The set of web hook handlers that are currently supported. Consult `Gitlab
# webhooks`_ documentation for complete list of webhooks and the payload
# details.
#
# The key should be the value GitLab uses in the "object_kind" parameter in
# the payload of the web hook request.
#
# .. _Gitlab webhooks:
# https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
WEBHOOKS = {
    "merge_request": process_merge_request,
}


def main():
    """Run main loop."""
    settings.QUEUE.consume_messages(
        settings.WEBHOOKS_EXCHANGE, os.environ['PUBLIC_ROUTING_KEYS'].split(),
        process_message, queue_name=os.environ.get('PUBLIC_QUEUE'))


if __name__ == "__main__":
    main()

"""Message queue helpers."""
from urllib import parse

import gitlab

from . import settings


class Message:
    """Webhook message."""

    def __init__(self, payload):
        """Initialize the instance."""
        self.payload = payload

    def gl_instance(self):
        """Return a Gitlab API instance."""
        return gitlab.Gitlab(
            self.gitlab_url(), private_token=self.token()
        )

    def token(self):
        """Return the api token."""
        return settings.GITLAB_TOKENS[self.gitlab_host()]

    def gitlab_url(self):
        """Return the GitLab URL."""
        if 'project' in self.payload:
            web_url = self.payload['project']['web_url']
        else:
            web_url = self.payload['repository']['homepage']
        web_url = parse.urlsplit(web_url)
        return parse.urlunparse(web_url[:2] + ('',) * 4)

    def gitlab_host(self):
        """Return the GitLab host name."""
        return parse.urlsplit(self.gitlab_url()).hostname

"""Query bugzilla for MRs."""
import json
import logging
from enum import IntEnum

import gitlab
import requests

from webhook import BUGZILLA_QUEUE
from webhook import settings
from webhook.public import process_message as public_message
from webhook.queue import Message

REPO = None

_log = logging.getLogger(__name__)
_log.setLevel(logging.INFO)


def extract_bzs(commit):
    """Extract BZs from the commit message."""
    mlines = commit.message.split('\n')
    for line in mlines:
        if line.startswith('BZ:'):
            line = line[3:]
            line = line.replace(',', ' ')
            return line.split()
    raise Exception('No BZs Found in commit ' + str(commit))


def extract_files(commit):
    """Extract the list of files from the commit."""
    filelist = []
    diffstats = commit.stats.files
    for k in diffstats:
        filelist.append(k)
    return filelist


def validate_bzs(mrstate, reviewed_items, merge_request):
    """Validate bugzilla references."""
    for i in reviewed_items:
        bzs_valid = []
        i['commit_valid'] = False
        if not i['bzs']:
            i['bzs'] = []
            continue
        i['commit_valid'] = True
        for j in i['bzs']:
            bz_properties = {'bz': j, 'notes': []}
            buginputs = {
                'package': 'kernel',
                'namespace': 'rpms',
                'ref': 'refs/heads/' + merge_request.target_branch,
                'commits': [{
                    'hexsha': str(i['commit']),
                    'files': i['files'],
                    'resolved': [str(j)],
                    'related': [],
                    'reverted': [],
                }]
            }
            print(json.dumps(buginputs, indent=2))
            # NEED TO FIND A BETTER WAY TO DO THIS - FIXME!
            bugresults = requests.post(
                "https://10.19.208.80/lookaside/gitbz-query.cgi",
                json=buginputs, verify=False)

            resjson = bugresults.json()
            print(json.dumps(resjson, indent=2))
            if resjson['result'] == 'ok':
                bz_properties['approved'] = True
            else:
                bz_properties['approved'] = False
                errorstring = resjson['error'].replace('\n', ' ')
                bz_properties['notes'].append(errorstring)
            if mrstate.flags['bzreportlogs']:
                bz_properties['logs'] = resjson['logs']

            bzs_valid.append(bz_properties)
        i['bzs'] = bzs_valid


# pylint: disable=too-many-branches
def print_bz_report(mrstate, project, merge_request, reviewed_items):
    """Print a bugzilla report."""
    mr_approved = True
    report = ""
    table = []
    for i in reviewed_items:
        if not i['commit_valid']:
            print("Commit is not valid")
            mr_approved = False
        if mr_approved:
            table.append([i['commit'], "", "", ""])
        else:
            table.append(
                [i['commit'], "", "",
                 "Commit contains no valid bzs, please add a line to the "
                 "changelog\nin the format BZ: <bugzilla>[,<bugzilla>...]"])
        for j in i['bzs']:
            if not j['approved']:
                mr_approved = False
            notestring = ""
            for note in j['notes']:
                notestring = notestring + note + "<br>"
            table.append(["", j['bz'], j['approved'], notestring])
    report = "Project: " + project.name_with_namespace + "   \n"
    report += "Project ID: " + str(project.id) + "   \n"
    report += "Merge Request ID: " + str(merge_request.iid) + "   \n"
    report += "Target Branch: " + merge_request.target_branch + "   \n"
    report += "Note that acceptance rules are based on target branch and   \n"
    report += "are established by Red Hat policy.  If a given bz fails   \n"
    report += "validation, please submit a query in that bugzilla   \n"
    report += " \n"
    report += "BZ READINESS REPORT:\n\n"
    report += "|Commit|BZ|Approved|Notes|\n"
    report += "|:------|:------|:------|:-------|\n"
    for row in table:
        report += "|"+str(row[0])+"|"+row[1]+"|"+str(row[2])+"|"+row[3]+"|\n"

    report += "\n\n"
    if mr_approved:
        report += "Merge Request passes bz validation\n"
    else:
        report += "\nMerge Request fails bz validation.  \n"
        report += " \n"
        report += "To request re-evalution after getting bz approval "
        report += "add a comment to this MR with only the text: "
        report += "request-bz-evaluation "

    if mrstate.flags['bzreportlogs']:
        report += "LOGS:   \n"
        for i in reviewed_items:
            for j in i['bzs']:
                report += j['logs'] + "   \n"
    return (report, mr_approved)


def check_on_bzs(mrstate, merge_request, project):
    """Check BZs."""
    review_lists = []
    REPO.remotes['origin'].fetch(
        f"merge-requests/{merge_request.iid}/head:{merge_request.iid}")
    for commit in merge_request.commits():
        commit = REPO.commit(commit.id)
        try:
            found_bzs = extract_bzs(commit)
        # pylint: disable=broad-except
        except Exception:
            found_bzs = []
        try:
            found_files = extract_files(commit)
        # pylint: disable=broad-except
        except Exception:
            found_files = []

        review_lists.append(
            {'commit': commit, 'bzs': found_bzs, 'files': found_files})
    validate_bzs(mrstate, review_lists, merge_request)
    try:
        REPO.git.branch("-D", str(merge_request.iid))
    # pylint: disable=broad-except
    except Exception as exception:
        print("Odd....Error in deleting head: " + str(exception))
    return print_bz_report(mrstate, project, merge_request, review_lists)


class State(IntEnum):
    """State machine states."""

    NEW = 0
    BZVALID = 1
    CIVALID = 2
    CIBZVALID = 3
    READYTOMERGE = 4
    MAINTOVERRIDE = 5
    UNKNOWN = 6


# pylint: disable=too-many-instance-attributes
class MRState:
    """Merge request state."""

    def __init__(self, lab, project, merge_request, payload):
        """Initialize the instance."""
        self.lab = lab
        self.project = project
        self.merge_request = merge_request
        self.payload = payload
        self.need_update = False
        # Set our initial state based on our bz and ci labels
        self.current_state = self.compute_label_state()
        self.next_state = State.UNKNOWN
        self.flags = {'bzreportlogs': False}

    def compute_label_state(self):
        """Compute the label state."""
        current_state = State.NEW
        dont_care_labels = ['maintainerOverride', 'readyForMerge',
                            'bzValidationFails']
        labels_to_states = [
            {'label': set([]), 'state': State.NEW},
            {'label': set(['bzValidated']), 'state': State.BZVALID},
            {'label': set(['passesCI']), 'state': State.CIVALID},
            {'label': set(['bzValidated', 'passesCI']),
             'state': State.CIBZVALID}
        ]
        # make a copy of our labels
        valid_labels = self.merge_request.labels.copy()
        # remoe the labels that aren't relevant right now
        for i in dont_care_labels:
            try:
                valid_labels.remove(i)
            # pylint: disable=broad-except
            except Exception:
                pass

        # Find the initial state we are in
        for i in labels_to_states:
            if set(valid_labels) == i['label']:
                current_state = i['state']
                break

        # Now we need to make some adjustments based on maintainerOverride and
        # ReadyForMerge If the maintainer override flag has been applied, then
        # we are in MAINTOVERRIDE state
        if 'maintainerOverride' in self.merge_request.labels:
            current_state = State.MAINTOVERRIDE
        # If the readyForMerge flag is set, then we are in ReadyToMerge state
        if 'readyForMerge' in self.merge_request.labels:
            current_state = State.READYTOMERGE

        return current_state

    def add_label(self, label):
        """Add a label."""
        if label not in self.merge_request.labels:
            self.merge_request.labels.append(label)
            self.need_update = True

    def remove_label(self, label):
        """Remove a label."""
        if label in self.merge_request.labels:
            self.merge_request.labels.remove(label)
            self.need_update = True

    def add_if_not_label(self, label, iflabel):
        """Conditionally add a label."""
        if iflabel not in self.merge_request.labels:
            if label not in self.merge_request.labels:
                self.merge_request.labels.append(label)
                self.need_update = True

    def remove_if_not_label(self, label, iflabel):
        """Conditionally remove a label."""
        if iflabel not in self.merge_request.labels:
            if label in self.merge_request.labels:
                self.merge_request.labels.remove(label)
                self.need_update = True

    def update_mr_state(self):
        """Update the state machine state."""
        if self.next_state == State.NEW:
            self.remove_label('bzValidated')
            self.remove_label('passesCI')
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
        elif self.next_state == State.BZVALID:
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
            self.remove_label('passesCI')
            self.add_label('bzValidated')
        elif self.next_state == State.CIVALID:
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
            self.remove_label('bzValidated')
            self.add_label('passesCI')
        elif self.next_state == State.CIBZVALID:
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
            self.add_label('passesCI')
            self.add_label('bzValidated')
        elif self.next_state == State.READYTOMERGE:
            for label in self.merge_request.labels:
                if label.startswith("MR-NACK:"):
                    print("Skipping setting of readytomerge due to nacks\n")
                    return
            self.add_label('readyForMerge')
        elif self.next_state == State.MAINTOVERRIDE:
            self.add_label('maintainerOverride')

        if self.need_update:
            self.merge_request.save()

    def force_mr_state(self, state):
        """Set the current state machine state."""
        self.current_state = state
        print("Forcing MR state back to " +
              str(self.current_state), flush=True)

    def update_current_state(self):
        """Update the current state machine state."""
        self.current_state = self.next_state
        print("Updating MR current state to " +
              str(self.current_state), flush=True)

    def compute_next_state(self):
        """Compute the next state machine state."""
        print("Computing next state from state " +
              str(self.current_state), flush=True)
        if self.current_state == State.NEW:
            self.__run_bz_validation()
            # need to move the state machine along so the ci state is correct
            self.update_current_state()
            self.__check_ci_result()
        elif self.current_state == State.BZVALID:
            self.__check_ci_result()
        elif self.current_state == State.CIVALID:
            self.__run_bz_validation()
        elif self.current_state == State.CIBZVALID:
            self.__check_approvals()
        elif self.current_state == State.READYTOMERGE:
            self.__check_approvals()
        elif self.current_state == State.MAINTOVERRIDE:
            self.__force_ready_to_merge()
        else:
            self.__log_unknown_state()

        print("Computed next state as " + str(self.next_state), flush=True)

    def __check_ci_result(self):
        print("Checking CI state\n", flush=True)
        self.next_state = self.current_state
        if self.payload['object_kind'] == 'pipeline':
            # We're running this because a pipeline completed
            print("pipeline state is " +
                  self.payload['object_attributes']['status'])
            if self.payload['object_attributes']['status'] == 'success':
                if self.current_state == State.BZVALID:
                    self.next_state = State.CIBZVALID
                else:
                    self.next_state = State.CIVALID
        else:
            print("Cehcking MR pipeline result\n", flush=True)
            # We're running because we got an MR update
            if self.merge_request.pipeline:
                if self.merge_request.pipeline['status'] == 'success':
                    if self.current_state == State.BZVALID:
                        self.next_state = State.CIBZVALID
                    else:
                        self.next_state = State.CIVALID
            else:
                print("Checking CI as MR with no pipeline!\n", flush=True)
                # No relevant pipeline status, keep our current state
                self.next_state = self.current_state

        if self.next_state == State.CIBZVALID:
            self.update_current_state()
            self.__check_approvals()

    def __run_bz_validation(self):
        self.next_state = self.current_state
        print("Running bz validation\n", flush=True)
        if 'bzValidationFails' in self.merge_request.labels:
            return
        (report, approved) = check_on_bzs(
            self, self.merge_request, self.project)
        self.merge_request.notes.create({'body': report})
        if approved:
            if self.current_state == State.CIVALID:
                self.next_state = State.CIBZVALID
            else:
                self.next_state = State.BZVALID
        else:
            print("WE FAILED BZ VALIDATION!  MARK US AS SUCH!\n")
            # This is not really part of the state machine, but rather a marker
            # to prevent us from having to create several more states to
            # consider
            self.merge_request.labels.append('bzValidationFails')
            self.need_update = True
        if self.next_state == State.CIBZVALID:
            self.update_current_state()
            self.__check_approvals()

    def __check_approvals(self):
        approvals = self.merge_request.approvals.get()
        print("We have " + str(len(approvals.approved_by)) + " approvals")
        if len(approvals.approved_by) >= 1:
            if self.current_state == State.CIBZVALID:
                # This MR is ready for merging
                print("Marking MR as ready")
                self.next_state = State.READYTOMERGE
            elif self.current_state == State.READYTOMERGE:
                self.next_state = State.READYTOMERGE
        else:
            if self.current_state == State.READYTOMERGE:
                if 'bzValidated' in self.merge_request.labels:
                    self.next_state = State.BZVALID
                    if 'passesCI' in self.merge_request.labels:
                        self.next_state = State.CIBZVALID
                elif 'passesCI' in self.merge_request.labels:
                    self.next_state = State.CIVALID
            else:
                self.next_state = self.current_state

    def __do_nothing(self):
        self.next_state = self.current_state

    def __force_ready_to_merge(self):
        self.next_state = State.READYTOMERGE

    def __log_unknown_state(self):
        print("MR " + str(self.merge_request.iid) + " IS IN UNKNOWN STATE")


def process_mr(message):
    """Process a merge request message."""
    payload = message.data()
    lab = gitlab.Gitlab(message.gitlab_url(), private_token=message.token())

    project = lab.projects.get(payload['project']['id'])
    merge_request = project.mergerequests.get(
        payload['object_attributes']['iid'])
    mrstate = MRState(lab, project, merge_request, payload)

    return mrstate


def process_note(message):
    """Process a note message."""
    payload = message.data()
    lab = gitlab.Gitlab(message.gitlab_url(), private_token=message.token())

    notetext = payload['object_attributes']['note']
    project = lab.projects.get(payload['project']['id'])
    merge_request = project.mergerequests.get(payload['merge_request']['iid'])
    mrstate = MRState(lab, project, merge_request, payload)

    if notetext.startswith("request-bz-evaluation"):
        if notetext == "request-bz-evaluation(logs)":
            print("Bz report requests full logs")
            mrstate.flags['bzreportlogs'] = True
        elif notetext != "request-bz-evaluation":
            # Do nothing if the text is not either of the above
            # 2 variants
            return None

        # Force a re-run of the bz validation
        try:
            print("Removing bzValidationFails\n", flush=True)
            merge_request.labels.remove('bzValidationFails')
        # pylint: disable=broad-except
        except Exception:
            pass
        mrstate.force_mr_state(State.NEW)
    elif notetext == "request-pipeline-evaluation":
        mrstate.force_mr_state(State.BZVALID)
    else:
        # This is a note we can ignore, just return here
        return None

    return mrstate


def process_pipeline(message):
    """Process a pipeline message."""
    payload = message.data()
    lab = gitlab.Gitlab(message.gitlab_url(), private_token=message.token())

    # its a pipeline event, apply the passesCI label if the pipeline
    # is successful
    project = lab.projects.get(payload['project']['id'])
    merge_request = project.mergerequests.get(payload['merge_request']['iid'])
    mrstate = MRState(lab, project, merge_request, payload)
    # Force our state back to a state that requires CI checking
    if mrstate.current_state == State.CIVALID:
        mrstate.force_mr_state(State.NEW)
    elif mrstate.current_state == State.CIBZVALID:
        mrstate.force_mr_state(State.BZVALID)

    return mrstate


def process_message(message):
    """Process one message from the queue."""
    payload = message.data()
    object_kind = payload.get('object_kind', None)
    mrstate = None

    if not object_kind:
        print("Missing object_kind in message")
        return 3

    if object_kind == 'pipeline':
        print("Checking pipeline request\n")
        # We don't care about this event if there isn't an associated merge
        # request
        if not payload['merge_request']:
            return 0
        mrstate = process_pipeline(message)

    elif object_kind == 'note':
        print("Checking note request\n")

        mrstate = process_note(message)

    elif object_kind == 'merge_request':
        mrstate = process_mr(message)

    else:
        print(f"Doing nothing with object kind {object_kind}")
        return 1

    if mrstate:
        mrstate.compute_next_state()
        mrstate.update_mr_state()

    return 0


def main():
    """Consume messages from the bugzilla queue."""
    with settings.QUEUE.connect() as channel:

        channel.queue_declare(queue=BUGZILLA_QUEUE)

        for method, _, body in channel.consume(queue=BUGZILLA_QUEUE,
                                               auto_ack=False):
            payload = json.loads(body)
            message = Message(payload)

            process_message(message)

            # avoid adding to 'public' queue and instead call directly.
            # This minimizes the race condition with adding labels to
            # the same MR iid as it is tied to the same queue event.
            public_message(method.routing_key, payload)

            channel.basic_ack(method.delivery_tag)


if __name__ == "__main__":
    main()

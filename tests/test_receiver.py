"""Gitlab api interaction tests."""
import json
import os
import unittest
from unittest import mock

from webhook import settings
from webhook.receiver import app

SECRET = 'secret'
STREAM_SECRET = 'stream_secret'
RHEL_SECRET = 'rhel_secret'


@mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': SECRET})
class TestWebhook(unittest.TestCase):
    """ Test webhook receiver."""

    def setUp(self):
        """SetUp class."""
        self.client = app.test_client()

    def _check_event(self, data, routing_key, connection):
        response = self.client.post(
            '/', json=data, headers={'X-Gitlab-Token': SECRET})

        self.assertEqual(response.status, '200 OK')
        connection().channel().basic_publish.assert_called_with(
            exchange=settings.WEBHOOKS_EXCHANGE,
            routing_key=routing_key,
            body=json.dumps(data))

    @mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    def test_job_event(self, connection):
        """Check routing of a job event."""
        self._check_event({
            'object_kind': 'build',
            'repository': {
                'homepage': 'https://host.name:1234/group/subgroup/project',
            }
        }, 'host.name.group.subgroup.project.build', connection)

    @mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    def test_pipeline_event(self, connection):
        """Check routing of a pipeline event."""
        self._check_event({
            'object_kind': 'pipeline',
            'project': {
                'web_url': 'https://host.name:1234/group/subgroup/project',
            },
        }, 'host.name.group.subgroup.project.pipeline', connection)

    def test_entrypoint_missing(self):
        """Check a missing entrypoint."""
        response = self.client.post('/non-existent', json={})
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_websecret_missing(self):
        """Check a missing websecret."""
        response = self.client.post('/', json={})
        self.assertEqual(response.status, '403 FORBIDDEN')

    def test_websecret_wrong(self):
        """Check a wrong websecret."""
        response = self.client.post(
            '/', json={}, headers={'X-Gitlab-Token': 'bad_secret'})
        self.assertEqual(response.status, '403 FORBIDDEN')

    @mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': ''})
    def test_websecret_env_missing(self):
        """Check a missing websecret env variable."""
        response = self.client.post('/', json={})
        self.assertEqual(response.status, '404 NOT FOUND')

"""Webhook interaction tests."""
import json
import os
import unittest
from unittest import mock

import webhook.public
import webhook.queue
import webhook.settings


@mock.patch.dict(webhook.settings.GITLAB_TOKENS, {'web.url': 'TOKEN'})
class TestPublic(unittest.TestCase):
    """ Test Webhook class."""

    # pylint: disable=no-self-use
    @mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    @mock.patch('webhook.public.process_message')
    @mock.patch.dict(os.environ, {'PUBLIC_ROUTING_KEYS': ''})
    def test_entrypoint(self, mocked_process, mocked_connection):
        """Check basic queue consumption."""

        # setup dummy consume data
        payload = {'foo': 'bar'}
        consume_value = [(mock.Mock(), 'foo', json.dumps(payload))]
        mocked_connection().channel().consume.return_value = consume_value

        webhook.public.main()

        mocked_process.assert_called()

    @mock.patch('webhook.queue.gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab):
        """Check handling an unsupported object kind."""
        self._test(mocked_gitlab, False, updated_payload={
            'object_kind': 'foo'})

    @mock.patch('webhook.queue.gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab):
        """Check handling of a merge request."""
        self._test(mocked_gitlab, True)

    def _test(self, mocked_gitlab, result, updated_payload=None):
        payload = {'object_kind': 'merge_request',
                   'project': {'id': 1,
                               'web_url': 'https://web.url/g/p'},
                   'object_attributes': {'iid': 2}
                   }
        payload.update(updated_payload or {})

        changes = {"labels": [], "changes": [{
            "old_path": "VERSION",
            "new_path": "VERSION",
            "a_mode": "100644",
            "b_mode": "100644",
            "diff":
            r"--- a/VERSION\ +++ b/VERSION\ @@ -1 +1 @@\ -1.9.7\ +1.9.8",
            "new_file": False,
            "renamed_file": False,
            "deleted_file": False
        }]}

        # setup dummy gitlab data
        projects = mock.Mock()
        merge_request = mock.Mock()
        mocked_gitlab().__enter__().projects.get.return_value = projects
        projects.mergerequests.get.return_value = merge_request
        merge_request.changes.return_value = changes
        merge_request.labels = []

        return_value = webhook.public.process_message('dummy', payload)

        if result:
            mocked_gitlab.assert_called_with(
                'https://web.url', private_token='TOKEN')
            mocked_gitlab().__enter__().projects.get.assert_called_with(1)
            projects.mergerequests.get.assert_called_with(2)
            self.assertTrue(return_value)
        else:
            self.assertFalse(return_value)
